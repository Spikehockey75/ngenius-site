import { NgeniusGamingPage } from './app.po';

describe('ngenius-gaming App', function() {
  let page: NgeniusGamingPage;

  beforeEach(() => {
    page = new NgeniusGamingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
